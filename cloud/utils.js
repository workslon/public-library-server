module.exports = {
  /**
   * Returns Record related data only removing service fileds like `createdAt`, etc.
   * @param  {Object} object
   * @return {Object} data    Record related data
   */
  getRecordData: function(object) {
    var serviceFields = ['objectId', 'runCloudCode', 'updatedAt', 'createdAt'];
    var data = {};

    Object.keys(object).forEach(function(attr) {
      if (!~serviceFields.indexOf(attr)) {
        data[attr] = object[attr];
      }
    });

    return data;
  },

  validate: function(data) {
    var obj = {};
    var errors = {};
    var isValid = true;

    // fill not presented fields with `undefined`
    Object.keys(this.properties).forEach(function (prop) {
      obj[prop] = data[prop];
    });

    Object.keys(obj).forEach((function (chunk) {
      var errorMessage = this.check(chunk, data[chunk]).message;
      if (errorMessage) {
        errors[chunk] = errorMessage;
        isValid = false;
      }
    }).bind(this));

    return {
      errors: errors,
      isValid: isValid
    };
  }
};