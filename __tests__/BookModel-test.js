jest.autoMockOff();
var BookModel = require('../cloud/Book.js');

describe('BookModel', function() {
  describe('validate()', function() {
    it('should return empty errors object if the data is valid', function() {
      var data = {
        isbn: '123456789X',
        title: 'Book',
        year: 2000
      };
      expect(BookModel.validate(data).errors).toEqual({});
    });

    it('should return `isValid: true` if the data is valid', function() {
      var data = {
        isbn: '123456789X',
        title: 'Book',
        year: '2000'
      };
      expect(BookModel.validate(data).isValid).toEqual(true);
    });

    it('should return an error object if the data is invalid', function() {
      var data = {
        isbn: '123', // NOT valid ISBN
        title: 'Book',
        year: 2000
      };
      expect(BookModel.validate(data).errors).toEqual({
        isbn: 'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'
      });
    });

    it('should return `isValid: false` if the data is invalid', function() {
      var data = {
        isbn: '123', // NOT valid ISBN
        title: 'Book',
        year: 2000
      };
      expect(BookModel.validate(data).isValid).toEqual(false);
    });
  });
});