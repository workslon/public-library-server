jest.autoMockOff();
var util = require('../cloud/util.js');

describe('util.js', function() {
  describe('getBookData()', function() {
    it('filters predefined non-model data fields out ("updatedAt", "createdAt", etc.)', function () {
      var rawData = {
        isbn: '123456789X',
        title: 'Book',
        year: 2000,
        objectId: 'XXX' // predefined non-model field
      };

      var resultData = {
        isbn: '123456789X',
        title: 'Book',
        year: 2000
      };

      expect(util.getBookData(rawData)).toEqual(resultData);
    });

    it('returns an empty object if there is no model-defined fields at all', function () {
      var rawData = {
        updatedAt: '123',
        createdAt: '123',
        objectId: 'XXX'
      };

      var resultData = {};

      expect(util.getBookData(rawData)).toEqual(resultData);
    });
  });
});